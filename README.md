# FaturAqui

Faturaqui RESTful API demos

Here you can find examples in several languages on how to integrate FaturAqui calls in your website, appplication or script.
You can find examples by language in existent directories, empy or inexistent ones will be available soon, please feel free to send us more suggestions in case of need.

 - Python
 - PHP
 - C#
 - Java
 - JS
 - Ruby
 - Excel
 - Google Calc

Please visit https://faturaqui.com for more information